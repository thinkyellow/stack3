# Smidswater Stack 3

## Stack command

Quick fix: package-lock.json is in the stack3 folder, remove this and run 
```
stack install
```


Install CLI command (once), go to the stack folder cd /stack3 and run
```
./stack install-cli
```

### Add to project existing project
```
stack init
```

### Add to new project
```
stack install
```

### Update stack
```
stack update
```
### Edit config (parent folder of stack3)
```
nano stack.config.js
```

### General tasks

#### Build
```
stack build
```

#### Watch
```
stack watch
```

#### Run stack together with Tailwind CSS
```
Run 'stack watch' in terminal tab, then open a new terminal tab and run the command below:
npx tailwindcss -i ./src/css/tailwindcss/tailwind.css -o ./src/css/tailwindcss/output.css --watch

Note: Tailwind must still be installed separately as outlined in https://tailwindcss.com/docs/installation

```

#### List of commands
```
stack help
```

## Old way

### Add git module
```
git submodule add git@bitbucket.org:smidswater/stack3.git stack3
```

### Update git module
```
git submodule update --init
```

### Install stack3
```
cd stack3
npm i
```

### Edit config (parent folder of stack3)
```
nano stack.config.js
```

### General tasks

#### Build
```
npm run build
```

#### Watch
```
npm run watch
```
