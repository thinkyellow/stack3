const gulp = require('gulp');
const gulpPostCSS = require('gulp-postcss');
const postCSSCached = require('postcss-cached');
const gulpSourceMaps = require('gulp-sourcemaps');
const babelify = require('babelify');
const fs = require('fs-extra');
const path = require('path');
const gulpImageMin = require('gulp-imagemin');
const imageMinPNGQuant = require('imagemin-pngquant');
const gulpRun = require('gulp-run');
const gulpChanged = require('gulp-changed');
const spawn = require('child_process').spawn;
const abos = require('abos');
let config = {};
try {
  config = Object.assign({}, config, JSON.parse(fs.readFileSync('./stack.config.abos')));
  if (fs.existsSync('../stack.config.abos')) {
    config = Object.assign({}, config, JSON.parse(fs.readFileSync('../stack.config.abos')));
  }
} catch (e) {
  console.log(e);
}
function symLink(source, dest) {
  try {
    fs.readlinkSync(dest);
  } catch (e) {
    fs.symlinkSync(source, dest);
  }
}
config = abos(config);
const clusterSrc = require('gulp-cluster-src')(gulp);
gulp.task('process:css', () => gulp.src(config.sourcePaths.css)
  .pipe(gulpSourceMaps.init())
  .pipe(gulpPostCSS({
    config: path.resolve('./postcss.config.js'),
    use: postCSSCached
  }))
  .pipe(gulpSourceMaps.write('.'))
  .pipe(gulp.dest(config.destination.css))
);

gulp.task('process:img', () => gulp.src(config.sourcePaths.img)
  .pipe(gulpChanged(config.destination.img))
  .pipe(gulpImageMin({
    progressive: true,
    svgoPlugins: [{
      removeViewBox: false
    }],
    use: [imageMinPNGQuant()]
  }))
  .pipe(gulp.dest(config.destination.img))
);
var browserify = require('persistify');
var through2 = require('through2').obj;
gulp.task('process:js', () => {
  var browserified = through2(function (file, enc, next) {
    var b = browserify(Object.assign({}, {
      entries: file.path,
      debug: true
    }), {
      watch: false
    });

    b.plugin(require.resolve('common-shakeify'));
    b.transform(babelify.configure({
      presets: [
        [require.resolve('@babel/preset-env'), {
          targets: {
            browsers: ['IE 10', '> 1%', 'last 2 versions']
          }
        }],
        require.resolve('@babel/preset-flow')
      ],
      plugins: [
        [require.resolve('@babel/plugin-transform-runtime'), {"corejs": 2,
      "helpers": true,
      "regenerator": true,
      "useESModules":false }],
        [require.resolve('babel-plugin-inline-json-import'),{}],
        // Stage 0
        require.resolve("@babel/plugin-proposal-function-bind"),

        // Stage 1
        require.resolve("@babel/plugin-proposal-export-default-from"),
        require.resolve("@babel/plugin-proposal-logical-assignment-operators"),
        [require.resolve("@babel/plugin-proposal-optional-chaining"), {
          "loose": false
        }],
        [require.resolve("@babel/plugin-proposal-pipeline-operator"), {
          "proposal": "minimal"
        }],
        [require.resolve("@babel/plugin-proposal-nullish-coalescing-operator"), {
          "loose": false
        }],
        require.resolve("@babel/plugin-proposal-do-expressions"),
        // Stage 2
        [require.resolve("@babel/plugin-proposal-decorators"), {
          "legacy": true
        }],
        require.resolve("@babel/plugin-proposal-function-sent"),
        require.resolve("@babel/plugin-proposal-export-namespace-from"),
        require.resolve("@babel/plugin-proposal-numeric-separator"),
        require.resolve("@babel/plugin-proposal-throw-expressions"),

        // Stage 3
        require.resolve("@babel/plugin-syntax-dynamic-import"),
        require.resolve("@babel/plugin-syntax-import-meta"), [require.resolve("@babel/plugin-proposal-class-properties"), {
          "loose": false
        }],
        require.resolve("@babel/plugin-proposal-json-strings"),
        require.resolve('@babel/plugin-proposal-object-rest-spread'),
        [require.resolve("babel-plugin-console-source"), {
            "fullPath": false
        }],
        [require.resolve('babel-plugin-holes'), {}]
      ]
    }), {
      sourceMaps: true
    })
    b.transform(require.resolve('browserify-cdnjs'));
    b.transform(require.resolve('uglifyify'), {
      global: true
    });
    file.contents = b.bundle();
    this.push(file);
    next();
  });
  return gulp.src([config.sourcePaths.js, config.sourcePaths.excludeVendor], {
      read: false,
      taskName: 'process:js'
    }).pipe(browserified)
    .pipe(gulp.dest(config.destination.js));
});


gulp.task('copy:vendorJS', () => gulp.src(config.sourcePaths.vendor)
  .pipe(gulp.dest(config.destination.vendor)));

gulp.task('copy:fonts', () => gulp.src(config.sourcePaths.fonts)
  .pipe(gulp.dest(config.destination.fonts)));

gulp.task('init', (done) => {
  symLink('stack3/.eslintrc.js', '../.eslintrc.js');
  symLink('stack3/.editorconfig', '../.editorconfig');
  symLink('stack3/stylelint.config.js', '../stylelint.config.js');
  if (!fs.existsSync('../stack.config.abos')) {
    fs.copySync('stack.config.abos', '../stack.config.abos');
  }
  let packageJSONExists = fs.existsSync(config.source.packageJSON);
  if (!packageJSONExists) {
    fs.copySync('./_package.json', config.source.packageJSON);
  }


  const npmi = spawn('npm', ['install'], {
    cwd: config.source.packageJSONParent
  });

  npmi.stdout.on('data', (data) => {
    console.log(`${data}`);
  });

  npmi.stderr.on('data', (data) => {
    console.log(`${data}`);
  });

  npmi.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
    done();
  });
});
gulp.task('watch:css', () => {
  gulp.watch([config.sourcePaths.allCss, path.resolve('./postcss.config.js')], gulp.parallel('process:css'));
});

gulp.task('watch:fonts', () => {
  gulp.watch([config.sourcePaths.fonts], gulp.parallel('copy:fonts'));
});

gulp.task('watch:img', () => {
  gulp.watch([config.sourcePaths.img], gulp.parallel('process:img'));
});

gulp.task('watch:js', () => {
  gulp.watch([config.sourcePaths.allJs, config.sourcePaths.excludeVendor], gulp.parallel('process:js'));
  gulp.watch([config.sourcePaths.vendor], gulp.parallel('copy:vendorJS'));
});
gulp.task('build', gulp.parallel('copy:fonts', 'copy:vendorJS', 'process:css', 'process:js', 'process:img'));
gulp.task('watch', gulp.parallel('watch:css', 'watch:js', 'watch:img', 'watch:fonts', 'build'));


gulp.task('default', gulp.series('watch'));
