module.exports = {
  parser: require('postcss-comment-2'),
  plugins: [
    require('postcss-at-rules-variables'),
    require('postcss-each'),
    require('postcss-mixins'),
    require('postcss-import')({
      plugins: [
        require('postcss-at-rules-variables')(),
        require('postcss-import')
      ]
    }),
    require('postcss-for'),
    require('@smidswater/postcss-conditionals'),
    require('postcss-custom-properties'),
    require('postcss-nested'),
    require('postcss-custom-media')(),
    require('postcss-css-variables')(),
    require('postcss-calc')(),
    require('postcss-media-minmax'),
    require('postcss-ellipsis'),
    require('postcss-color-function'),
    require('postcss-cssnext')({
      browsers: ['IE 9', '>1%', 'last 2 versions'],
      feature: {
        autoprefixer: {
          grid: true
        }
      }
    }),
    require('postcss-selector-not'),
    require('postcss-font-variant'),
    require('postcss-input-range')(),
    require('postcss-hocus'),
    require('postcss-unique-selectors'),
    require('postcss-discard-empty'),
    require('postcss-discard-duplicates'),
    require('postcss-discard-comments'),
    require('postcss-discard-unused'),
    require('postcss-brand-colors'),
    require('postcss-responsive-font'),
    require('postcss-responsive-type'),
    require('postcss-aspect-ratio'),
    require('postcss-clear-fix'),
    require('postcss-morphicon'),
    require('postcss-random')({
      randomSeed:		0,
      round: 			true,
      noSeed: 		true,
      floatingPoint: 	0
    }),
    require('postcss-csso')()
  ]
};
